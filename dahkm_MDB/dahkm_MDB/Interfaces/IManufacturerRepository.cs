﻿using dahkm_MDB.API.Domain.Models.Entities;
using System.Collections.Generic;

namespace dahkm_MDB.Interfaces
{
    public interface IManufacturerRepository
    {
        IEnumerable<Manufacturer> GetManufacturers();
        Manufacturer GetManufacturer(int id);
        void DeleteManufacturer(int id);
        void UpdateManufacturer(int id);
        void SaveManufacturer(Manufacturer manufacturer);
    }
}

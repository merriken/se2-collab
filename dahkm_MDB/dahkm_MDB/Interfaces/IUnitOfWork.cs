﻿namespace dahkm_MDB.Interfaces
{
    public interface IUnitOfWork
    {
        void SaveChanges();
    }
}

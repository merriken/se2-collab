﻿using dahkm_MDB.API.Domain.Models.Entities;
using System.Collections.Generic;

namespace dahkm_MDB.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Transactions> GetProducts();
        Transactions GetProduct(int id);
        void DeleteProduct(int id);
        void UpdateProduct(int id);
        void SaveProduct(Transactions product);
    }
}

﻿using dahkm_MDB.API.Domain.Models.Entities;
using System.Collections.Generic;

namespace dahkm_MDB.Interfaces
{
    public interface ILoginAttemptsRepository
    {
        IEnumerable<LoginAttempts> GetLoginAttempts();
        LoginAttempts GetLoginAttempt(int id);
        void DeleteLoginAttempt(int id);
        void UpdateLoginAttempt(int id);
        void SaveLoginAttempt(LoginAttempts loginAttampt);
    }
}

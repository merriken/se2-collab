﻿using dahkm_MDB.API.Domain.Models.Entities;
using System.Collections.Generic;

namespace dahkm_MDB.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<BikeSalesType> GetOrders();
        BikeSalesType GetOrder(int id);
        void DeleteOrder(int id);
        void UpdateOrder(int id);
        void SaveOrder(BikeSalesType order);
    }
}

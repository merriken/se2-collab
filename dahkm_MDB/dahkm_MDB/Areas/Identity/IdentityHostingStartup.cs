﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(dahkm_MDB.Areas.Identity.IdentityHostingStartup))]
namespace dahkm_MDB.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
            });
        }
    }
}
﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.Interfaces;
using System.Collections.Generic;

namespace dahkm_MDB.API.Domain.Models.Services
{
    public class LoginAttemptsService : ILoginAttemptsService
    {
        private readonly ILoginAttemptsRepository _loginRepo;
        private readonly IUnitOfWork _unitOfWork;
        public LoginAttemptsService(ILoginAttemptsRepository loginRepo, IUnitOfWork unitOfWork)
        {
            _loginRepo = loginRepo;
            _unitOfWork = unitOfWork;
        }
        public void DeleteLoginAttempt(int id)
        {
            _loginRepo.DeleteLoginAttempt(id);
            _unitOfWork.SaveChanges();
        }

        public LoginAttempts GetLoginAttempt(int id)
        {
            return _loginRepo.GetLoginAttempt(id);
        }

        public IEnumerable<LoginAttempts> GetLoginAttempts()
        {
            return _loginRepo.GetLoginAttempts();
        }

        public void SaveLoginAttempt(LoginAttempts LoginAttempts)
        {
            _loginRepo.SaveLoginAttempt(LoginAttempts);
            _unitOfWork.SaveChanges();
        }

        public void UpdateLoginAttempt(int id)
        {
            _loginRepo.UpdateLoginAttempt(id);
            _unitOfWork.SaveChanges();
        }
    }
}

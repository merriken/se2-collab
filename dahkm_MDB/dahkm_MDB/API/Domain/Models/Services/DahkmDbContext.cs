﻿using dahkm_MDB.API.Domain.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace dahkm_MDB.API.Domain.Models.Services
{
    public class DahkmDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        public DahkmDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<BikeSalesType> Orders { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Metrics> Metrics { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<LoginAttempts> LoginAttempts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //Metrics
            builder.Entity<Metrics>().ToTable("Metrics");
            builder.Entity<Metrics>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Metrics>().Property(p => p.Date).IsRequired();
            builder.Entity<Metrics>().Property(p => p.ClicksAM);
            builder.Entity<Metrics>().Property(p => p.ClicksAfternoon);
            builder.Entity<Metrics>().Property(p => p.ClicksPM);
            builder.Entity<Metrics>().Property(p => p.ConversionRate);

            //Customer
            builder.Entity<Customer>().ToTable("Customers");
            builder.Entity<Customer>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Customer>().Property(p => p.PhoneNumber);
            builder.Entity<Customer>().Property(p => p.FirstName);
            builder.Entity<Customer>().Property(p => p.LastName);
            builder.Entity<Customer>().Property(p => p.Address);
            builder.Entity<Customer>().Property(p => p.BalanceDue);
            builder.Entity<Customer>().Property(p => p.CityId);
            builder.Entity<Customer>().Property(p => p.ZipCode);

            //Manufacturer
            builder.Entity<Manufacturer>().ToTable("Manufacturers");
            builder.Entity<Manufacturer>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Manufacturer>().Property(p => p.PhoneNumber);
            builder.Entity<Manufacturer>().Property(p => p.Name);
            builder.Entity<Manufacturer>().Property(p => p.StreetAddress);
            builder.Entity<Manufacturer>().Property(p => p.ZipCode);
            builder.Entity<Manufacturer>().Property(p => p.City);

            //Order
            builder.Entity<BikeSalesType>().ToTable("BikeSalesType");
            builder.Entity<BikeSalesType>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<BikeSalesType>().Property(p => p.modelType);

            //Product
            builder.Entity<Transactions>().ToTable("Transactions");
            builder.Entity<Transactions>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Transactions>().Property(p => p.Quantity);

            //LoginAttempts
            builder.Entity<LoginAttempts>().ToTable("LoginAttempts");
            builder.Entity<LoginAttempts>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<LoginAttempts>().Property(p => p.Username);
            builder.Entity<LoginAttempts>().Property(p => p.Successful);

            //Vendor
            builder.Entity<Vendor>().ToTable("Vendors");
            builder.Entity<Vendor>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Vendor>().Property(p => p.StreetAddress);
            builder.Entity<Vendor>().Property(p => p.PhoneNumber);
            builder.Entity<Vendor>().Property(p => p.Name);
            builder.Entity<Vendor>().Property(p => p.ZipCode);
            builder.Entity<Vendor>().Property(p => p.City);


            builder.Entity<Metrics>().HasData
            (
                new Metrics { Id = 39253928, Date = new DateTime(2019, 12, 4), ClicksAM = 5, ClicksAfternoon = 10, ClicksPM = 13, ConversionRate = 2.2m }

            );
        }
    }
}

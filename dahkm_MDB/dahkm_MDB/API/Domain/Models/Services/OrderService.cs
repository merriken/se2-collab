﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.Interfaces;
using System.Collections.Generic;

namespace dahkm_MDB.API.Domain.Models.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepo;
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IOrderRepository orderRepo, IUnitOfWork unitOfWork)
        {
            _orderRepo = orderRepo;
            _unitOfWork = unitOfWork;
        }
        public void DeleteOrder(int id)
        {
            _orderRepo.DeleteOrder(id);
            _unitOfWork.SaveChanges();
        }

        public BikeSalesType GetOrder(int id)
        {
            return _orderRepo.GetOrder(id);
        }

        public IEnumerable<BikeSalesType> GetOrders()
        {
            return _orderRepo.GetOrders();
        }

        public void SaveOrder(BikeSalesType order)
        {
            _orderRepo.SaveOrder(order);
            _unitOfWork.SaveChanges();
        }

        public void UpdateOrder(int id)
        {
            _orderRepo.UpdateOrder(id);
            _unitOfWork.SaveChanges();
        }
    }
}

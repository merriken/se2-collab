﻿namespace dahkm_MDB.API.Domain.Models.Entities
{
    public class LoginAttempts
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public bool Successful { get; set; }
    }
}

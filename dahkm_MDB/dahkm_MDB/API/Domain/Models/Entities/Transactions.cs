﻿namespace dahkm_MDB.API.Domain.Models.Entities
{
    public class Transactions
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
    }
}

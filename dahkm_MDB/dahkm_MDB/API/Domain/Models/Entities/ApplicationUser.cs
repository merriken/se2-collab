﻿using Microsoft.AspNetCore.Identity;

namespace dahkm_MDB.API.Domain.Models.Entities
{
    public class ApplicationUser : IdentityUser
    {
    }
}

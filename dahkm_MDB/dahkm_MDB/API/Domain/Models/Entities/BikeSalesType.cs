﻿namespace dahkm_MDB.API.Domain.Models.Entities
{
    public class BikeSalesType
    {
        public int Id { get; set; }
        public string modelType { get; set; }
    }
}
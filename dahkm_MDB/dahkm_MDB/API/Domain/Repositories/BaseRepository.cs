﻿using dahkm_MDB.API.Domain.Models.Services;

namespace dahkm_MDB.API.Domain.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly DahkmDbContext _context;

        public BaseRepository(DahkmDbContext context)
        {
            _context = context;
        }
    }
}

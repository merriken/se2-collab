﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.API.Domain.Models.Services;
using dahkm_MDB.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dahkm_MDB.API.Domain.Repositories
{
    public class LoginAttemptsRepository : BaseRepository, ILoginAttemptsRepository
    {

        public LoginAttemptsRepository(DahkmDbContext context) : base(context)
        {
        }

        public void DeleteLoginAttempt(int id)
        {
            LoginAttempts loginAttempts = _context.Find<LoginAttempts>(id);
            _context.Remove(loginAttempts);
        }

        public LoginAttempts GetLoginAttempt(int id)
        {
            return _context.Find<LoginAttempts>(id);
        }

        public IEnumerable<LoginAttempts> GetLoginAttempts()
        {
            return _context.LoginAttempts.ToList<LoginAttempts>();
        }

        public void SaveLoginAttempt(LoginAttempts LoginAttempts)
        {
            _context.Add(LoginAttempts);
        }

        public void UpdateLoginAttempt(int id)
        {
            LoginAttempts loginAttempts = _context.Find<LoginAttempts>(id);
            _context.Update<LoginAttempts>(loginAttempts);
        }
    }
}

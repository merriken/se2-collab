﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.API.Domain.Models.Services;
using dahkm_MDB.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dahkm_MDB.API.Domain.Repositories
{
    public class OrderRepository : BaseRepository, IOrderRepository
    {
        public OrderRepository(DahkmDbContext context) : base(context)
        {

        }

        public void DeleteOrder(int id)
        {
            BikeSalesType order = _context.Find<BikeSalesType>(id);
            _context.Remove(order);
        }

        public BikeSalesType GetOrder(int id)
        {
            return _context.Find<BikeSalesType>(id);
        }

        public IEnumerable<BikeSalesType> GetOrders()
        {
            return _context.Orders.ToList<BikeSalesType>();
        }

        public void SaveOrder(BikeSalesType order)
        {
            _context.Add(order);
        }

        public void UpdateOrder(int id)
        {
            BikeSalesType order = _context.Find<BikeSalesType>(id);
            _context.Update<BikeSalesType>(order);
        }
    }
}

﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.API.Domain.Models.Services;
using dahkm_MDB.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dahkm_MDB.API.Domain.Repositories
{
    public class ProductRepository : BaseRepository, IProductRepository
    {
        public ProductRepository(DahkmDbContext context) : base(context)
        {
        }

        public void DeleteProduct(int id)
        {
            Transactions product = _context.Find<Transactions>(id);
            _context.Remove(product);
        }

        public Transactions GetProduct(int id)
        {
            return _context.Find<Transactions>(id);
        }

        public IEnumerable<Transactions> GetProducts()
        {
            return _context.Transactions.ToList<Transactions>();
        }

        public void SaveProduct(Transactions product)
        {
            _context.Add(product);
        }

        public void UpdateProduct(int id)
        {
            Transactions product = _context.Find<Transactions>(id);
            _context.Update<Transactions>(product);
        }
    }
}

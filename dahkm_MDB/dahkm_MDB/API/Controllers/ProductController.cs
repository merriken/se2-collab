﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace dahkm_MDB.API.Controllers
{
    [Route("Transactions")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        /*        // GET: api/Product
                [HttpGet]
                public IEnumerable<Transactions> Get()
                {
                    return _productService.GetProducts();
                }*/

        // GET: api/Product/5
        [HttpGet("{id}")]
        public Transactions Get(int id)
        {
            return _productService.GetProduct(id);
        }

        public IActionResult Index()
        {
            var transactions = _productService.GetProducts();

            return View(transactions);
        }

        // POST: api/Product
        [HttpPost]
        public ActionResult<Transactions> Post([FromBody]Transactions product)
        {

            _productService.SaveProduct(product);
            return Ok();
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public ActionResult<Transactions> Put([FromBody]Transactions product)
        {

            var existingProduct = _productService.GetProduct(product.Id);
            if (existingProduct != null)
            {
                existingProduct.Quantity = product.Quantity;

                _productService.UpdateProduct(existingProduct.Id);
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _productService.DeleteProduct(id);
        }
    }
}

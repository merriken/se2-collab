﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace dahkm_MDB.API.Controllers
{
    [Route("api/VendorController")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        private readonly IVendorService _vendorService;

        public VendorController(IVendorService vendorService)
        {
            _vendorService = vendorService;
        }
        // GET: api/Vendor
        [HttpGet]
        public IEnumerable<Vendor> Get()
        {
            return _vendorService.GetVendors();
        }

        // GET: api/Vendor/5
        [HttpGet("{id}")]
        public Vendor Get(int id)
        {
            return _vendorService.GetVendor(id);
        }

        // POST: api/Vendor
        [HttpPost]
        public ActionResult<Vendor> Post([FromBody]Vendor vendor)
        {

            var newVendor = new Vendor();
            newVendor.City = vendor.City;
            newVendor.Name = vendor.Name;
            newVendor.PhoneNumber = vendor.PhoneNumber;
            newVendor.StreetAddress = vendor.StreetAddress;
            newVendor.ZipCode = vendor.ZipCode;

            _vendorService.SaveVendor(newVendor);
            return Ok();
        }

        // PUT: api/Vendor/5
        [HttpPut("{id}")]
        public ActionResult<Vendor> Put([FromBody]Vendor vendor)
        {


            var existingVendor = _vendorService.GetVendor(vendor.Id);
            if (existingVendor != null)
            {
                existingVendor.City = vendor.City;
                existingVendor.Name = vendor.Name;
                existingVendor.PhoneNumber = vendor.PhoneNumber;
                existingVendor.StreetAddress = vendor.StreetAddress;
                existingVendor.ZipCode = vendor.ZipCode;

                _vendorService.UpdateVendor(existingVendor.Id);
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _vendorService.DeleteVendor(id);
        }
    }
}

﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace dahkm_MDB.API.Controllers
{
    [Route("BikeSalesType")]
    [ApiController]
    public class BikeSalesTypeController : Controller
    {
        private readonly IOrderService _orderService;

        public BikeSalesTypeController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: api/Order
        /*        [HttpGet]
                public IEnumerable<BikeSalesType> Get()
                {
                    return _orderService.GetOrders();
                }*/

        public IActionResult Index()
        {
            var orders = _orderService.GetOrders();

            return View(orders);
        }

        // GET: api/BikeSalesType/5
        [HttpGet("{id}")]
        public BikeSalesType Get(int id)
        {
            return _orderService.GetOrder(id);
        }

        // POST: api/BikeSalesType
        [HttpPost]
        public ActionResult<BikeSalesType> Post([FromBody]BikeSalesType order)
        {
            _orderService.SaveOrder(order);
            return Ok();
        }

        // PUT: api/BikeSalesType/5
        [HttpPut("{id}")]
        public ActionResult<BikeSalesType> Put([FromBody] BikeSalesType order)
        {


            var existingOrder = _orderService.GetOrder(order.Id);
            if (existingOrder != null)
            {
                existingOrder.modelType = order.modelType;


                _orderService.UpdateOrder(existingOrder.Id);
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _orderService.DeleteOrder(id);
        }
    }

}

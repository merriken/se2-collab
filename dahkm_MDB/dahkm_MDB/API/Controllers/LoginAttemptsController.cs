﻿using dahkm_MDB.API.Domain.Models.Entities;
using dahkm_MDB.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace dahkm_MDB.API.Controllers
{
    [Route("LoginAttempts")]
    [ApiController]
    public class LoginAttemptsController : Controller
    {
        private readonly ILoginAttemptsService _loginservice;

        public LoginAttemptsController(ILoginAttemptsService loginService)
        {
            _loginservice = loginService;
        }

        /*        // GET: api/Order
                [HttpGet]
                public IEnumerable<LoginAttempts> Get()
                {
                    return _loginservice.GetLoginAttempts();
                }*/

        public IActionResult Index()
        {
            var attempts = _loginservice.GetLoginAttempts();

            return View(attempts);
        }

        // GET: api/BikeSalesType/5
        [HttpGet("{id}")]
        public LoginAttempts Get(int id)
        {
            return _loginservice.GetLoginAttempt(id);
        }

        // POST: api/BikeSalesType
        [HttpPost]
        public ActionResult<LoginAttempts> Post([FromBody]LoginAttempts login)
        {
            _loginservice.SaveLoginAttempt(login);
            return Ok();
        }

        // PUT: api/BikeSalesType/5
        [HttpPut("{id}")]
        public ActionResult<LoginAttempts> Put([FromBody] LoginAttempts login)
        {


            var existingLogin = _loginservice.GetLoginAttempt(login.Id);
            if (existingLogin != null)
            {
                existingLogin.Successful = login.Successful;
                existingLogin.Username = login.Username;


                _loginservice.UpdateLoginAttempt(existingLogin.Id);
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _loginservice.DeleteLoginAttempt(id);
        }
    }
}
